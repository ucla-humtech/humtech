<?php
/*
 Template Name: Faculty Upgrade Page
*/
?>
<?php get_header(); ?>

			<div class="content">
				<div class="col" id="main-content" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1 class="page-title"><?php the_title(); ?></h1>
						<section>
							<?php the_content(); ?>
							<?php 
							if ($_GET['pass'] == "df53ca268240ca76670c8566ee54568a" ) { ?>
								<!-- Calendly link widget begin -->
								<link href="https://assets.calendly.com/assets/external/widget.css" rel="stylesheet">
								<script src="https://assets.calendly.com/assets/external/widget.js" type="text/javascript"></script>
								<a href="" onclick="Calendly.initPopupWidget({url: 'https://calendly.com/humtech/upgrade'});return false;" class="btn">Select Pickup Time</a>
								<!-- Calendly link widget end -->
							<? } else { ?>
							<div class="notice">
								<h3>URL Token Required</h3>
								<p>You do not have the correct URL token to access the scheduling form. Please click on the link you received in your email.</p>
								</div>
							<? } ?>
							
							
						</section>
					</article>

				<?php endwhile; else : ?>

					<article id="post-not-found" <?php post_class(); ?> role="article">
						<h1 class="page-title">Page Not Found</h1>
						<section>
							<p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

				<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>