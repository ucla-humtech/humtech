<?php get_header(); ?>
			<div class="content main">
				<div class="col" id="main-content" role="main">

					<h1>The HumTech Blog</h1>
					<div class="blog-container">
						<?php 
							$counter = 0; 
							if (have_posts()) : while (have_posts()) : the_post(); 
							$counter++; // add +1 to count for each post ?>
	
						<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">
							<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'content-width' ); ?></a>
							<a href="<?php the_permalink() ?>" rel="bookmark"><h3 class="entry-title"><?php the_title(); ?></h3></a>
							<span class="entry-details"><strong>by</strong> <?php the_author_posts_link(); ?> <strong>in</strong> 
							<?php the_category( ', ' ); ?></span>
							<section class="entry-content">
								<p><?php
								$content = get_the_excerpt();
								$trimmed_content = wp_trim_words( $content, 20, '...' );
								echo $trimmed_content;
								?></p>
								<?php if (!is_paged()) {
									if($counter===1) { ?>
									<a href="<?php the_permalink() ?>" class="btn large">Read More</a>
									<?php } else { } 
								}?>
							</section>
						</article>
	
						<?php endwhile; ?>
					</div>
					
					<?php bones_page_navi(); ?>
					
					<?php else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<section>
							<p>There is nothing available to show here at this time. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

					<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>