<?php
/*
	Basic Navigation and Hero 
*/
?>
<?php if(get_field('enable_banner', 'option') == "enable") { ?>
	<?php if(get_field('banner_content', 'option')) { ?>
		<div class="site-banner">
			<div class="content">
				<?php the_field('banner_content', 'option'); ?>
			</div>
		</div>
	<?php } ?>
<?php }?>
<a href="#main-content" class="hidden skip">Skip to main content</a>
<div id="container">
	<header role="banner" class="top">
		<div class="content">
			<a href="<?php echo home_url(); ?>" rel="nofollow">
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-logo.png" alt="UCLA" class="university-logo" /><span class="hidden">UCLA</span>
				<?php // H1 if homepage, H2 otherwise.
					if ( is_front_page() ) { ?>
				<h1><img src="<?php echo get_template_directory_uri(); ?>/library/images/dept-logo.png" alt="<?php the_field('department_name', 'option'); ?>" class="dept-logo" /><span class="hidden"><?php the_field('department_name', 'option'); ?></span></h1>
				<?php } else { ?>
				<h2><img src="<?php echo get_template_directory_uri(); ?>/library/images/dept-logo.png" alt="<?php the_field('department_name', 'option'); ?>" class="dept-logo" /><span class="hidden"><?php the_field('department_name', 'option'); ?></span></h2>
				<?php }?>
			</a>
			<?php if(get_field('enable_donation', 'option') == "enable") { ?>
			<div class="give-back">
				<?php if(get_field('link_type', 'option') == "internal") { ?>
				<a href="<?php the_field('donation_page', 'option'); ?>" class="btn give">
				<?php }?>
				<?php if(get_field('link_type', 'option') == "external") { ?>
				<a href="<?php the_field('donation_link', 'option'); ?>" class="btn give" target="_blank">
				<?php }?><span class="fa fa-heart" aria-hidden="true"></span>
				<?php the_field('button_text', 'option'); ?></a>
				<?php if(get_field('supporting_text', 'option')) { ?>
				<span class="support-tagline"><?php the_field('supporting_text', 'option'); ?></span>
				<?php }?>
			</div>
			<?php }?>
		</div>
		<nav role="navigation" aria-labelledby="main navigation" class="desktop">
			<?php wp_nav_menu(array(
				'container' => false,
				'menu' => __( 'Main Menu', 'bonestheme' ),
				'menu_class' => 'main-nav',
				'theme_location' => 'main-nav',
				'before' => '',
				'after' => '',
				'depth' => 2,
			)); ?>
		</nav>
		<?php get_search_form(); ?>
	</header>
	<?php 
		// Add breadcrumbs to homepage
		if ( is_front_page() ) { 
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
			}
		}
		// Breadcrumb everywhere else
		elseif ( is_single() || is_category() || is_archive() ) { 
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
			}
		}
		else {
			// Only show hero image on a page or post
			if ( has_post_thumbnail() && is_single() || has_post_thumbnail() && is_page() ) {
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'hero' );
				$url = $thumb['0']; ?>
	<div id="hero" style="background-image: url('<?=$url?>');">
		<?php // if support landing page
		if ( has_post_thumbnail() && is_page('1475') ) { ?>
		<div class="text">
			<div class="content">
				<h1><?php the_title(); ?></h1>
				<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
					<label class="hidden">Search</label>
					<input type="hidden" name="cat" id="cat" value="1" />
					<input type="search" class="search" placeholder="<?php echo esc_attr_x( 'How can we help you?', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
				    <button type="submit" class="search-btn"><i class="fa fa-search"></i></button>
				</form>
			</div>
		</div>
		<?php } ?>
		&nbsp;
	</div>
	<?php }
			// And show breadcrumb
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
			} 
		} 
	?>