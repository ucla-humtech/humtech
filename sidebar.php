				<?php // For Event month or list landing page, https://gist.github.com/jo-snips/2415009
				// Only run if The Events Calendar is installed 
				if ( tribe_is_past() || tribe_is_upcoming() && !is_tax() || tribe_is_month() && !is_tax() || is_singular( 'tribe_events' ) ) { ?>
					<div class="col side feed" role="complementary">
						<?php if ( is_active_sidebar( 'events-sidebar' ) ) :
							dynamic_sidebar( 'events-sidebar' );
						else : endif; ?>
					</div>
				<?php }
				// For posts, events, search, and author pages
				elseif (is_singular('post') || is_category() || is_search() || is_home() || is_author() ) { ?>
				<div class="col side feed" role="complementary">
					<?php
						wp_nav_menu(array(
							'container' => false,
							'menu' => __( 'Blog Categories', 'bonestheme' ),
							'menu_class' => 'blog-nav',
							'theme_location' => 'blog-nav',
							'before' => '',
							'after' => '',
							'depth' => 2,
							'items_wrap' => '<h3>Blog Categories</h3> <ul>%3$s</ul>'
						));
					?>
					<?php if ( is_active_sidebar( 'news-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'news-sidebar' ); ?>
					<?php else : endif; ?>
				</div>
				<?php } ?>
				<?php // For pages and custom post types
				if (is_page() || is_404() || is_singular('projects')) { ?>
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php
								// If a Support subpage
								if (get_field('menu_select') == "support") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Support', 'bonestheme' ),
										'menu_class' => 'support-nav',
										'theme_location' => 'support-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Support</h3> <ul>%3$s</ul>'
									));
								}
								// If a Computer Support subpage
								if (is_tree(939) || get_field('menu_select') == "computer") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Computer Support', 'bonestheme' ),
									   	'menu_class' => 'computing-nav',
									   	'theme_location' => 'computing-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Computer Support</h3> <ul>%3$s</ul>'
									));
								}
								// If an Instructional Support subpage
								if (is_tree(4490) || get_field('menu_select') == "instructional") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Instructional Support', 'bonestheme' ),
									   	'menu_class' => 'instructional-nav',
									   	'theme_location' => 'instructional-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Instructional Support</h3> <ul>%3$s</ul>'
									));
								}
								// If a Web Support subpage
								if (is_tree(4572) || get_field('menu_select') == "web") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Web Support', 'bonestheme' ),
									   	'menu_class' => 'web-nav',
									   	'theme_location' => 'web-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Web Support</h3> <ul>%3$s</ul>'
									));
								}
								// If a Project page
								if (is_singular('projects') || get_field('menu_select') == "projects") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Projects', 'bonestheme' ),
									   	'menu_class' => 'projects-nav',
									   	'theme_location' => 'projects-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Projects</h3> <ul>%3$s</ul>'
									));
								}
								// If a Spaces page
								if (is_tree(4684) || get_field('menu_select') == "labs") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Learning Spaces', 'bonestheme' ),
									   	'menu_class' => 'spaces-nav',
									   	'theme_location' => 'spaces-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Learning Spaces</h3> <ul>%3$s</ul>'
									));
								}
								// For Search, 404's, or other pages you want to use it on
								// Replace 9999 with id of parent page
								if (is_search() || is_404() || is_page('contact') || is_page('about') || is_page('digital-humanities') || get_field('menu_select') == "general") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Main Menu', 'bonestheme' ),
										'menu_class' => 'side-nav',
										'theme_location' => 'main-nav',
										'before' => '',
										'after' => '',
										'depth' => 1,
										'items_wrap' => '<h3>Main Menu</h3> <ul>%3$s</ul>'
									));
								}
							?>
						</nav>
						<?php
						if (get_field('sidebar') == "instructional") {
							dynamic_sidebar( 'instructional-sidebar' );
						}
						if (get_field('sidebar') == "web") {
							dynamic_sidebar( 'web-sidebar' );
						}
						if (get_field('sidebar') == "computing") {
							dynamic_sidebar( 'computing-sidebar' );
						}
						if (get_field('sidebar') == "labs") {
							dynamic_sidebar( 'labs-sidebar' );
						}
						if (get_field('sidebar') == "video") {
							dynamic_sidebar( 'video-sidebar' );
						}
						?>
					</div>
				</div>
				<?php } ?>