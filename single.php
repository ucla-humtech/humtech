<?php get_header(); ?>

			<div class="content">
				<div class="col" id="main-content" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
						<section class="entry-content cf" itemprop="articleBody">
							<?php the_post_thumbnail( 'content-width' ); ?>
							<p class="by-line">By <?php the_author_posts_link(); ?> on <?php echo get_the_date(); ?></p>
							<?php the_content(); ?>
						</section>
						<?php 
						if(get_field('show_author_bio') == 'yes') {
							if ( get_the_author_meta('description') ) : ?>
							<section class="about-author">
								<h3>About the Author</h3>
								<p><?php the_author_meta('description'); ?></p>
							</section>
							<?php endif; ?>
						<?php } ?>
					</article>

				<?php endwhile; else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

				<?php endif; ?>
				
				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>