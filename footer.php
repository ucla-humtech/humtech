			<footer role="contentinfo">
				<div class="content">
					<nav role="navigation" aria-labelledby="footer navigation">
						<?php 
						// If there is a menu set for the footer use that
						if ( has_nav_menu( 'footer-nav' ) ) {
							wp_nav_menu(array(
								'container' => '',
								'menu' => __( 'Footer Menu', 'bonestheme' ),
								'menu_class' => 'footer-nav',
								'theme_location' => 'footer-nav',
								'before' => '',
								'after' => '',
								'depth' => 1,
							));
						} else {
						// If not, use the main menu
							wp_nav_menu(array(
								'container' => '',
								'menu' => __( 'Main Menu', 'bonestheme' ),
								'menu_class' => 'footer-nav',
								'theme_location' => 'main-nav',
								'before' => '',
								'after' => '',
								'depth' => 1,
							));
						} ?>
						<ul class="social-links">
						<?php if(get_field('facebook', 'option')) { ?>
							<li class="icon"><a href="<?php the_field('facebook', 'option'); ?>"><span class="fa fa-facebook-official" aria-hidden="true"><span class="hidden">Facebook</span></span></a></li>
						<?php } if(get_field('twitter', 'option')) { ?>
							<li class="icon"><a href="<?php the_field('twitter', 'option'); ?>"><span class="fa fa-twitter" aria-hidden="true"><span class="hidden">Twitter</span></span></a></li>
						<?php } if(get_field('instagram', 'option')) { ?>
							<li class="icon"><a href="<?php the_field('instagram', 'option'); ?>"><span class="fa fa-instagram" aria-hidden="true"><span class="hidden">Instagram</span></span></a></li>
						<?php } if(get_field('contact_us', 'option')) { ?>
							<li class="icon"><a href="<?php the_field('contact_us', 'option'); ?>"><span class="fa fa-envelope"><span class="hidden">Contact Us</span></span></a></li>
						<?php } ?>
						</ul>
					</nav>
					<?php get_search_form(); ?>
					<div class="copyright">
						<a href="http://www.ucla.edu" class="university-logo"><img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-logo-white.png" alt="UCLA" /></a>
						<p>
						<?php if (get_field('department_name', 'option')) { ?>	
						<?php the_field('department_name', 'option'); ?> is part of the <a href="http://humanities.ucla.edu">Humanities Division</a> within <a href="http://www.college.ucla.edu/">UCLA College</a>.<br />
						<?php } 
						if (get_field('main_office_location', 'option')) { ?>	
						<?php the_field('main_office_location', 'option'); ?> <span>|</span> <?php the_field('city_state', 'option'); ?> <?php the_field('zip_code', 'option'); ?> 
						<?php }
						if (get_field('phone_number', 'option')) { ?>	
						<span>|</span> <strong>P:</strong> <?php the_field('phone_number', 'option'); ?>
						<?php } 
						if (get_field('fax_number', 'option')) { ?>	
						<span>|</span> <strong>F:</strong> <?php the_field('fax_number', 'option'); ?>
						<?php } 
						if (get_field('dept_email_address', 'option')) {
						$department_email = antispambot(get_field('dept_email_address', 'option')); ?>
						<span>|</span> <strong>E:</strong> <a href="mailto:<?php echo $department_email; ?>"><?php echo $department_email; ?></a>
						<?php } ?>
						<br />
						University of California &copy; <?php echo date('Y'); ?> UC Regents</p>
					</div>
				</div>
			</footer>
		<?php wp_footer(); ?>
	</body>
</html>