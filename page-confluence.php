<?php
/*
 Template Name: Confluence Page
*/
?>
<?php get_header(); ?>

			<div class="content">
				<div class="col" id="main-content" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1 class="page-title"><?php the_title(); ?></h1>
						<?php if(get_field('confluence_page_id')) { ?>
							<?php 
								$conf_id = get_field('confluence_page_id');
							?>
							<section>
								<?php the_content(); ?>
							</section>
							<section>
								<?php 
								require_once 'Unirest.php';
								$headers = array('Accept' => 'application/json');
								$response = Unirest\Request::get(
								'https://humtech.atlassian.net/wiki/rest/api/content/' . $conf_id . '?expand=body.styled_view',
								$headers
								);
								$content = $response->body;
								echo $content->body->styled_view->value;
								?>
							</section>
						<?php } ?>
						
					</article>

				<?php endwhile; else : ?>

					<article id="post-not-found" <?php post_class(); ?> role="article">
						<h1 class="page-title">Page Not Found</h1>
						<section>
							<p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

				<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>