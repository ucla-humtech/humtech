<?php
// Project Post Type Settings

// add custom categories
register_taxonomy( 'project_cat', 
	array('projects'), /* if you change the name of register_post_type( 'project', then you have to change this */
	array('hierarchical' => true,     /* if this is true, it acts like categories */
		'labels' => array(
			'name' => __( 'Project Categories', 'bonestheme' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Project Category', 'bonestheme' ), /* single taxonomy name */
			'search_items' =>  __( 'Search Project Categories', 'bonestheme' ), /* search title for taxomony */
			'all_items' => __( 'All Project Categories', 'bonestheme' ), /* all title for taxonomies */
			'parent_item' => __( 'Parent Project Category', 'bonestheme' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'Parent Project Category:', 'bonestheme' ), /* parent taxonomy title */
			'edit_item' => __( 'Edit Project Category', 'bonestheme' ), /* edit custom taxonomy title */
			'update_item' => __( 'Update Project Category', 'bonestheme' ), /* update title for taxonomy */
			'add_new_item' => __( 'Add New Project Category', 'bonestheme' ), /* add new title for taxonomy */
			'new_item_name' => __( 'New Project Category Name', 'bonestheme' ) /* name title for taxonomy */
		),
		'show_admin_column' => true, 
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 
			'slug' => 'projects',
			'with_front' => false,
		)
	)
);

// add custom tags
register_taxonomy( 'project_type', 
	array('projects'), /* if you change the name of register_post_type( 'courses_type', then you have to change this */
	array('hierarchical' => false,    /* if this is false, it acts like tags */
		'labels' => array(
			'name' => __( 'Project Type', 'bonestheme' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Project Type', 'bonestheme' ), /* single taxonomy name */
			'search_items' =>  __( 'Search Project Types', 'bonestheme' ), /* search title for taxomony */
			'all_items' => __( 'All Project Types', 'bonestheme' ), /* all title for taxonomies */
			'parent_item' => __( 'Parent Project Type', 'bonestheme' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'Parent Project Type:', 'bonestheme' ), /* parent taxonomy title */
			'edit_item' => __( 'Edit Project Type', 'bonestheme' ), /* edit custom taxonomy title */
			'update_item' => __( 'Update Project Type', 'bonestheme' ), /* update title for taxonomy */
			'add_new_item' => __( 'Add New Project Type', 'bonestheme' ), /* add new title for taxonomy */
			'new_item_name' => __( 'New Project Type Name', 'bonestheme' ) /* name title for taxonomy */
		),
		'show_tagcloud' => false,
		'show_ui' => true,
		'query_var' => true,
	)
);

// let's create the function for the custom type
function project_post_type() { 
	// creating (registering) the custom type 
	register_post_type( 'projects', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Projects', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Project', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Projects', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Project', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Project', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Project', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Project', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Projects', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'No project added yet.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Contains all projects', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 5, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-layout', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'project', 'with_front' => true ), /* you can specify its url slug */
			'capability_type' => 'post',
			'hierarchical' => true,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'revisions', 'thumbnail' )
			
		) /* end of options */
	); /* end of register post type */
}

// adding the function to the Wordpress init
add_action( 'init', 'project_post_type');	
?>