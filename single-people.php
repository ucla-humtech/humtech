<?php get_header(); ?>
			<div class="content main">
				<?php if(get_field('photo')) {
					$image = get_field('photo');
					if( !empty($image) ): 
					// vars
					$url = $image['url'];
					$title = $image['title'];
					// thumbnail
					$size = 'people-large';
					$thumb = $image['sizes'][ $size ];
					$width = $image['sizes'][ $size . '-width' ];
					$height = $image['sizes'][ $size . '-height' ];
				endif; ?>
				<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
				<?php } else { ?>
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="Silhouette" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
				<?php } ?>
				<div class="col" id="main-content" role="main">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article">
						<h1><?php the_title(); ?></h1>
						<?php if(get_field('position_title')) { ?>
						<span class="position"><?php the_field('position_title'); ?></span>
						<?php } ?>
						<div class="details">
						<?php if(get_field('email_address')) { ?>
							<?php $person_email = antispambot(get_field('email_address')); ?>
							<span class="email"><strong>E-mail:</strong> <a href="mailto:<?php echo $person_email; ?>"><?php echo $person_email; ?></a></span>
						<?php } ?>
						<?php if(get_field('phone_number')) { ?>
							<span class="phone"><strong>Phone: </strong><?php the_field('phone_number'); ?></span>
						<?php } ?>
						<?php if(get_field('office')) { ?>
							<span class="office"><strong>Office: </strong><?php the_field('office'); ?></span>
						<?php } ?>
						</div>
						<?php if(get_field('linkedin') || get_field('facebook') || get_field('twitter') || get_field('instagram')) { ?>
						<ul class="social">
						<?php if(get_field('linkedin')) { ?>
							<li class="icon"><a href="<?php the_field('linkedin'); ?>"><span class="fa fa-linkedin" aria-hidden="true"><span class="hidden">LinkedIn</span></span></a></li>
						<?php } ?>
						<?php if(get_field('facebook')) { ?>
							<li class="icon"><a href="<?php the_field('facebook'); ?>"><span class="fa fa-facebook" aria-hidden="true"><span class="hidden">Facebook</span></span></a></li>
						<?php } ?>
						<?php if(get_field('twitter')) { ?>
							<li class="icon"><a href="<?php the_field('twitter'); ?>"><span class="fa fa-twitter" aria-hidden="true"><span class="hidden">Twitter</span></span></a></li>
						<?php } ?>
						<?php if(get_field('instagram')) { ?>
							<li class="icon"><a href="<?php the_field('instagram'); ?>"><span class="fa fa-instagram" aria-hidden="true"><span class="hidden">Instagram</span></span></a></li>
						<?php } ?>
						</ul>
						<?php } ?>
						<section class="bio">
							<?php the_content(); ?>
						</section>
						<?php if(get_field('skills')) { ?>
						<section id="skills">
							<h2>Skills</h2>
							<?php the_field('skills'); ?>
						</section>
						<?php } ?>
						<?php if(get_field('publications')) { ?>
						<section id="publications">
							<h2>Publications</h2>
							<?php the_field('publications'); ?>
						</section>
						<?php } ?>
						<?php if(get_field('presentations')) { ?>
						<section id="presentations">
							<h2>Presentations</h2>
							<?php the_field('presentations'); ?>
						</section>
						<?php } ?>
						<?php if(get_field('wordpress_user')) { 
						$wordpress_user = get_field('wordpress_user');
						$wordpress_user_id = $wordpress_user['ID'];
						?>
						<?php $user_loop = new WP_Query( array( 'author' => $wordpress_user_id, 'post_type' => 'post', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'ASC')); ?>
						<?php if( $user_loop->have_posts() ): ?>
						<section id="posts">
							<h2>Blog Posts</h2>
							<ul>
							<?php while ( $user_loop->have_posts() ) : $user_loop->the_post(); ?>
								<li>
									<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
								</li>
							<?php endwhile; ?>
							</ul>
						</section>
						<?php endif; 
						} ?>
						<?php if(get_field('custom_section_title')) { ?>
						<section id="other">
							<h2><?php the_field('custom_section_title'); ?></h2>
							<?php the_field('custom_section_content'); ?>
						</section>
						<?php } ?>
					</article>
					<?php endwhile; ?>
					<?php else : endif; ?>
				</div>	
			</div>
<?php get_footer(); ?>