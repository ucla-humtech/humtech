<?php
/*
 Template Name: Project Listing
*/
?>
<?php get_header(); ?>
			<header id="main-content" class="main">
				<div class="content">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
					<?php // Select what people category to show
					$project_category = get_field('project_category');
					if( $project_category ) {
						$project_cat = $project_category->slug;
					}
					?>
				</div>
				<?php if ( get_field('display_filters') == 'show' ) { ?>
				<?php if ( has_nav_menu('faculty-filter') ) { ?> 
				<div class="filter">
				<?php // To make another filter, duplicate the div below ?>
					<div class="options button-group" data-filter-group="field">
					<?php if(get_field('filter_label')) { ?>
						<h3><?php the_field('filter_label'); ?></h3>
					<?php } ?>
						<button data-filter="" data-text="All" class="option all is-checked">All</button>
						<?php wp_nav_menu(array(
							'container' => false,
							'menu' => __( 'Project Filter', 'bonestheme' ),
							'menu_class' => 'project-filter',
							'theme_location' => 'project-filter',
							'before' => '',
							'after' => '',
							'depth' => 1,
							'items_wrap' => '%3$s',
							'walker' => new Filter_Walker
						)); ?>
					</div>
				</div>
				<?php wp_nav_menu( array(
					'menu' => 'Project Filter',
					'items_wrap' => '<form class="dropdown-filter"><select><option value="*">View All</option>%3$s</select></form>',
					'walker' => new Dropdown_Walker()
				)); ?>
				<h2 class="filter-title">All</h2>
				<?php }
			} ?>
			</header>
			<div class="content">
				<div class="filter-list">
					<ul class="<?php echo $project_cat ?>">
					<?php $core_loop = new WP_Query( array( 'project_cat' => $project_cat, 'post_type' => 'projects', 'posts_per_page' => -1, 'orderby' => 'date')); ?>
					<?php while ( $core_loop->have_posts() ) : $core_loop->the_post(); ?>
						<li class="filter-item<?php $project_type = get_field('project_type'); if( $project_type ): foreach( $project_type as $type ): ?> <?php echo $type->slug; ?><?php endforeach; endif;?>">
							<a href="<?php the_permalink() ?>">
								<?php if ( has_post_thumbnail() ) {
										$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'project-small' );
										$url = $thumb['0']; ?>
										<img src="<?=$url?>" class="project_image" alt="<?php the_title(); ?>" />
									<?php } else { ?>
								        <img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-placeholder.jpg" class="project_image" alt="A placeholder photo for <?php the_title(); ?>" />
								<?php } ?>
							</a>
							<dl>
								<dt class="name"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></dt>
								<?php if(get_field('project_description')) { ?>
								<dd class="short_description"><?php
								$content = get_field('project_description');
								$trimmed_content = wp_trim_words( $content, 19, '...' );
								echo $trimmed_content;
								?></dd>
								<?php }
								if(get_field('principal_investigator_field')) { ?>
								<dd class="principal_investigator"><strong>Principal Investigator(s): </strong>
								
								<?php
								$content_pi = get_field('principal_investigator_field');
								$trimmed_content_pi = wp_trim_words( $content_pi, 15, '...' );
								echo $trimmed_content_pi;
								?></dd>
								<?php } ?>
							</dl>
						</li>
					<?php endwhile; ?>					
					</ul>					
				</div>
			</div>
<?php get_footer(); ?>