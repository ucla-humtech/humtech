<div class="col news-col full">
	<h3><?php the_sub_field('posts_title'); ?></h3>
	<?php 
		$term = get_sub_field('category');
		$amount = 4;
		$posts_query = new WP_Query( array( 'showposts' => $amount, 'cat' => $term->term_id ) );
	?>
	<?php if( $term ) { ?>
	<a class="view-all" href="/category/<?php echo $term->slug; ?>/">View all <?php echo $term->name; ?></a>
	<?php } else { ?>
	<a class="view-all" href="/blog">View all posts</a>
	<?php } ?>
	<?php  ?>
	<ol>
		<?php if ($posts_query->have_posts()) : while ($posts_query->have_posts()) : $posts_query->the_post(); ?>
		<li>
			<a href="<?php the_permalink() ?>">
			<?php if ( has_post_thumbnail() ) {
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'content-width' );
				$url = $thumb['0']; ?>
				<img src="<?=$url?>" alt="<?php the_title(); ?>" />
			<?php } else { ?>
		        <img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-thumb.jpg" alt="A photo of <?php the_title(); ?>" />
			<?php } ?>
			</a>
			<div class="item">
				<a href="<?php the_permalink() ?>">
					<h4><?php the_title(); ?></h4>
				</a>
				<span class="by-line">By <span class="author"><?php the_author_posts_link(); ?></span></span>
				<p>
					<?php $content = get_the_excerpt();
					$trimmed_content = wp_trim_words( $content, 20, '...' );
					echo $trimmed_content; ?>
				</p>
			</div>
		</li>
		<?php endwhile; ?>
	</ol>
	<?php endif; ?>
	<?php wp_reset_postdata(); ?>
</div>