<div class="col notice-col <?php the_sub_field('notice_width'); ?>">
	<h3><?php the_sub_field('notice_title'); ?></h3>
	<?php the_sub_field('notice_content'); ?>
	<?php if(get_sub_field('notice_show_button') == "yes") { ?>
	<a class="btn" href="<?php the_sub_field('notice_button_link'); ?>"><?php the_sub_field('notice_button_text'); ?></a>
	<?php } ?>
</div>