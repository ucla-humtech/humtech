<div class="col content-col full">
	<h3><?php the_sub_field('content_title'); ?></h3>
	<?php the_sub_field('content'); ?>
	<?php if(get_sub_field('show_button') == "yes") { ?>
	<a class="btn" href="<?php the_sub_field('button_link'); ?>"><?php the_sub_field('button_text'); ?></a>
	<?php } ?>
</div>