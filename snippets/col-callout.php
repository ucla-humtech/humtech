<div class="col callout-col full">
	<?php if( have_rows('callout') ): ?>
	
		<ul>
	
		<?php while( have_rows('callout') ): the_row(); 
	
			// vars
			$callout_title = get_sub_field('callout_title');
			$callout_image = get_sub_field('callout_image');
			$callout_icon = get_sub_field('callout_icon');
			$callout_description = get_sub_field('callout_description');
			$callout_link = get_sub_field('callout_link');
			$callout_button_text = get_sub_field('callout_button_text');
			
			if(!empty($callout_image)): 
				// vars
				$url = $callout_image['url'];
				$title = $callout_image['title'];
				// thumbnail
				$size = 'callout';
				$callout = $callout_image['sizes'][ $size ];
				$width = $callout_image['sizes'][ $size . '-width' ];
				$height = $callout_image['sizes'][ $size . '-height' ];
			endif;
			?>
	
			<li>
				<?php if( $callout_link ): ?>
				<a href="<?php echo $callout_link; ?>">
				<?php endif; ?>
					<div class="callout-header" style="background-image: url('<?php echo $callout; ?>');">
						<div class="callout-content">
							<?php if( $callout_title ): ?>
							<h3><?php echo $callout_title; ?></h3>
							<?php endif; ?>
							<?php if( $callout_icon ): ?>
							<span class="fa <?php echo $callout_icon; ?>"></span>
							<?php endif; ?>
						</div>
					</div>
				<?php if( $callout_link ): ?>
				</a>
				<?php endif; ?>
				<?php if( $callout_description ): ?>
			    	<p><?php echo $callout_description; ?></p>
			    <?php endif; ?>
			    <?php if( $callout_button_text ): ?>
			    <?php if( $callout_link ): ?>
			    <a href="<?php echo $callout_link; ?>" class="btn"><?php echo $callout_button_text; ?></a>
			    <?php endif; ?>
			    <?php endif; ?>
			</li>
	
		<?php endwhile; ?>
	
		</ul>
	
	<?php endif; ?>
	
	
	<?php wp_reset_postdata(); ?>
</div>