<div class="col events-col <?php the_sub_field('events_width'); ?>">
	<h3><?php the_sub_field('events_title'); ?></h3>
	<?php 
		$term = get_sub_field('category');
		$amount = get_sub_field('amount_to_show');
		if($term) { 
			$events_query = new WP_Query( array( 'post_type' => 'tribe_events', 'showposts' => $amount, 
			'tax_query' => array(
				array(
					'taxonomy' => 'tribe_events_cat',
					'terms' => array($term->term_id)
				)
			)
		) ); 
		} else {
			$events_query = new WP_Query( array( 'post_type' => 'tribe_events', 'showposts' => $amount ) );
		}
	?>
	<?php if( $term ) { ?>
	<a class="view-all" href="/events/category/<?php echo $term->slug; ?>/">View all events<span class="hidden">for <?php echo $term->name; ?></span></a>
	<?php } else { ?>
	<a class="view-all" href="/events/">View all events</a>
	<?php } ?>
	<ol>
		<?php if ($events_query->have_posts()) : while ($events_query->have_posts()) : $events_query->the_post(); ?>
		<li class="tribe-events-list-widget-events">		
			<a href="<?php the_permalink() ?>">
				<?php do_action( 'tribe_events_list_widget_before_the_event_title' ); ?>	
				<h4><?php the_title(); ?></h4>
				<?php do_action( 'tribe_events_list_widget_after_the_event_title' ); ?>
			</a>
			<?php do_action( 'tribe_events_list_widget_before_the_meta' ) ?>
			<div class="duration">
				<?php echo tribe_events_event_schedule_details(); ?>
			</div>
			<?php do_action( 'tribe_events_list_widget_after_the_meta' ) ?>
			<p>
				<?php $content = get_the_excerpt();
				$trimmed_content = wp_trim_words( $content, 20, '...' );
				echo $trimmed_content; ?>
			</p>
		</li>
		<?php endwhile; ?>
	</ol>
	<?php else : ?>
	<?php if( $term ) { ?>
	<p>There are no upcoming <?php echo $term->name; ?> events. Please check back soon.</p>
	<?php } else { ?>
	<p>There are no upcoming events. Please check back soon.</p>
	<?php } ?>
	<?php endif; ?>
	<?php wp_reset_postdata(); ?>
</div>