<?php
/*
 Template Name: Web Hosting Page
*/
?>
<?php get_header(); ?>

			<div class="content">
				<div class="col full-width" id="main-content" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<section class="top-content">
							<h1><?php the_title(); ?></h1>
							<?php the_content(); ?>
						</section>
						<?php
							if( have_rows('columns') ): ?>
								<section class="column-container">
									<div class="flex-columns">
									    <?php while( have_rows('columns') ) : the_row();
									
									        // Load sub field value.
									        $column = get_sub_field('column'); ?>
											
											<div class="flex-column">
												<?php echo $column ?>
											</div>
										
										<?php endwhile; ?>
									</div>
								</section>
							<?php else : endif; ?>
						<?php if(get_field('additional_content')) { ?>
						<section class="additional-content">
							<?php the_field('additional_content'); ?>
						</section>
						<?php } ?>
					</article>

				<?php endwhile; else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

				<?php endif; ?>

				</div>
			</div>

<?php get_footer(); ?>
