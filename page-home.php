<?php
/*
 Template Name: Home Page
*/
?>
<?php get_header(); ?>
			<div id="main-content" role="main">
				<div class="content">
					<?php // If set to Single/Random 
						if(get_field('hero_type', 'option') == "single") {
						$rows = get_field('hero_image'); // get all the rows
						$rand_row = $rows[ array_rand($rows) ]; // get a random row
						$silder_image = $rand_row['image']; // get the sub field value
						$slider_title = $rand_row['title']; // get the sub field value 
						$slider_description = $rand_row['description']; // get the sub field value 
						//$slider_button = $rand_row['button_text']; // get the sub field value 
						$slider_link = $rand_row['link']; // get the sub field value 			
						if(!empty($silder_image)): 
							// vars
							$url = $silder_image['url'];
							$title = $silder_image['title'];
							// thumbnail
							$size = 'home-hero';
							$slide = $silder_image['sizes'][ $size ];
							$width = $silder_image['sizes'][ $size . '-width' ];
							$height = $silder_image['sizes'][ $size . '-height' ];
						endif;
					?>
					<?php if($silder_image): ?>
					<?php if($slider_link): ?>
					<a href="<?php echo $slider_link; ?>" class="hero-link">
					<?php endif; ?>
						<div id="hero" class="desktop" style="background-image: url('<?php echo $slide; ?>');">
							<div class="content <?php if($slider_title || $slider_description): ?>text<?php endif; ?>">
							<?php if($slider_title || $slider_description): ?>
								<div class="hero-description <?php the_field('vertical_text_alignment', 'option'); ?>">
									<div class="content <?php the_field('horizontal_text_alignment', 'option'); ?>">
										<h2><?php echo $slider_title; ?></h2>
										<?php if($slider_description): ?>
										<p><?php echo $slider_description; ?></p>
										<?php endif; ?>
									</div>
								</div>
							<?php endif; ?>
							</div>
						</div>
					<?php if($slider_link): ?>
					</a>
					<?php endif; ?>
					<?php endif; ?>
					<?php } ?>
					
					<?php // If set to Slider
					if(get_field('hero_type', 'option') == "slider") { ?>
					<script type="text/javascript">
						jQuery("document").ready(function($) {
							$(document).ready(function(){
							  $('#bxslider').bxSlider({
							  	autoHover: true,
							  	auto: false,
							  });
							});
						});
					</script>
					<div id="slider">
						<ul id="bxslider">
							<?php if(have_rows('hero_image')): ?>
							<?php while(have_rows('hero_image')): the_row(); ?>
							<?php
								$slider_title = get_sub_field('title');
								$slider_description = get_sub_field('description');
								$slider_link = get_sub_field('link');
								$silder_image = get_sub_field('image');
								//$slider_button = get_sub_field('button_text');
								if(!empty($silder_image)): 
									// vars
									$url = $silder_image['url'];
									$title = $silder_image['title'];
									// thumbnail
									$size = 'home-hero';
									$slide = $silder_image['sizes'][ $size ];
									$width = $silder_image['sizes'][ $size . '-width' ];
									$height = $silder_image['sizes'][ $size . '-height' ];
								endif;
							?>		
							<?php if($slider_link): ?>
							<a href="<?php echo $slider_link; ?>" class="hero-link">
							<?php endif; ?>
								<li style="background-image: url('<?php echo $slide; ?>');">
									<div class="bg">
										<div class="content">
											<div class="hero-description <?php the_field('vertical_text_alignment', 'option'); ?>">
												<div class="content <?php the_field('horizontal_text_alignment', 'option'); ?>">
													<?php if($slider_title): ?>
													<h2><?php echo $slider_title; ?></h2>
													<?php endif; ?>
													<?php if($slider_description): ?>
													<p><?php echo $slider_description; ?></p>
													<?php endif; ?>
												</div>
											</div>
										</div>
									</div>
								</li>
							<?php if($slider_link): ?>
							</a>
							<?php endif; ?>
							<?php endwhile; ?>
							<?php endif; ?>
						</ul>
					</div>
					<?php } ?>
					<?php if(get_field('notice_title') || get_field('notice_description')) { ?>
					<div class="col notice-col one">
						<?php if(get_field('notice_title')) { ?>
						<h3><?php the_field('notice_title'); ?></h3>
						<?php } ?>
						<?php if(get_field('notice_description')) { ?>
						<?php the_field('notice_description'); ?>
						<?php } ?>
					</div>
					<?php } ?>
					<?php
					if(have_rows('homepage_columns')) :
						while (have_rows('homepage_columns')) : the_row();
							
							// For showing snippet from any page
							if(get_row_layout() == 'page_excerpt') 
								get_template_part('snippets/col', 'page');
					        
							// For showing list of recent post
							elseif(get_row_layout() == 'recent_posts') 
								get_template_part('snippets/col', 'posts');
							
							// For showing free form content
							elseif(get_row_layout() == 'content_block') 
								get_template_part('snippets/col', 'content');
							
							// For showing list of events from event widget
							elseif(get_row_layout() == 'upcoming_events') 
								get_template_part('snippets/col', 'events');
							
							// For showing a menu
							elseif(get_row_layout() == 'menu') 
								get_template_part('snippets/col', 'menu');
							
							// For showing a menu
							elseif(get_row_layout() == 'section_callouts') 
								get_template_part('snippets/col', 'callout');
								
							// For showing a notice
							elseif(get_row_layout() == 'notice') 
								get_template_part('snippets/col', 'notice');
							
						endwhile;
					endif;
			    	?>
				</div>					
			</div>
		</div>
<?php get_footer(); ?>