<?php
/*
 Template Name: Support Directions Template
*/
?>
<?php get_header(); ?>

			<div class="content">
				<div class="col" id="main-content" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1><?php the_title(); ?></h1>
						<section>
							<?php the_content(); ?>
							<?php if( have_rows('steps') ): ?>
							
								<ul>
							
								<?php while( have_rows('steps') ): the_row(); 
									// vars
									$step_title = get_sub_field('step_title');
									$item_string = str_replace(array(".", ",", "-", ":", "(", ")", "'"," "), '' , $step_title);
									?>
							
									<li class="step">
									<?php if( $step_title ): ?>
										<a href="#<?php echo $item_string; ?>"><?php echo $step_title; ?></a>
									<?php endif; ?>
									</li>
							
								<?php endwhile; ?>
							
								</ul>
							
							<?php endif; ?>
						</section>
						<section class="steps">
						<?php if( have_rows('steps') ): ?>
						
							<ul>
						
							<?php while( have_rows('steps') ): the_row(); 
								// vars
								$step_title = get_sub_field('step_title');
								$item_string = str_replace(array(".", ",", "-", ":", "(", ")", "'"," "), '' , $step_title);
								$step_description = get_sub_field('step_description');
								$step_image = get_sub_field('step_image');
								if(get_sub_field('step_image')) {
									if( !empty($step_image) ): 
										// vars
										$url = $image['url'];
										$title = $image['title'];
										// thumbnail
										$size = 'content-width';
										$thumb = $image['sizes'][ $size ];
										$width = $image['sizes'][ $size . '-width' ];
										$height = $image['sizes'][ $size . '-height' ];
									endif; 
								} ?>
								<li class="step" id="<?php echo $item_string; ?>">
								<?php if( $step_title ): ?>
									<h3><?php echo $step_title; ?></h3>
								<?php endif; ?>
								<?php if( $step_description ): ?>
								    <?php echo $step_description; ?>
								<?php endif; ?>
								<?php if( $step_image ): ?>
									<a href="<?php echo $step_image['url']; ?>" data-rel=”lightbox”><img src="<?php echo $step_image['url']; ?>" alt="<?php echo $step_image['alt'] ?>" /></a>
								<?php endif; ?>
								</li>
						
							<?php endwhile; ?>
						
							</ul>
						
						<?php endif; ?>
						</section>
					</article>

				<?php endwhile; else : endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>