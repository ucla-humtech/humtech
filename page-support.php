<?php
/*
 Template Name: Support Landing Template
*/
?>
<?php get_header(); ?>

			<div class="col" id="main-content" role="main">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<section class="support-desc">
					<?php the_content(); ?>
				</section>
				<?php if( have_rows('support_column') ): ?>
				<section class="support">
					<div class="content">
					<?php while( have_rows('support_column') ): the_row(); 
						// vars
						$support_title = get_sub_field('support_title');
						$support_link = get_sub_field('support_link');
						$support_description = get_sub_field('support_description');
						$common_issues = get_sub_field('common_issues');
						?>
						<div class="support-column">
						<?php if( $support_title ): ?>
							<?php if( $support_link ): ?><a href="<?php echo $support_link; ?>"><?php endif; ?><h3><?php echo $support_title; ?></h3><?php if( $support_link ): ?></a><?php endif; ?>
						<?php endif; ?>
						<?php if( $support_description ): ?>
						    <?php echo $support_description; ?>
						<?php endif; ?>
						<?php if( $common_issues ): ?>
						    <?php echo $common_issues; ?>
						<?php endif; ?>
						<?php if( $support_link ): ?>
							<a href="<?php echo $support_link; ?>" class="btn">Read More</a>
						<?php endif; ?>
						</div>
					<?php endwhile; ?>
					</div>
				</section>
				
				<?php endif; ?>
					
				<section class="contact-section">
					<div class="content">
					<?php if(get_field('contact_title')) { ?>
						<h3><?php the_field('contact_title'); ?></h3>
					<?php } ?>
					<?php if(get_field('contact_description')) { ?>
						<div class="support-desc">
							<?php the_field('contact_description'); ?>
						</div>
					<?php } ?>
					<?php if( have_rows('contact_column') ): ?>
						<div class="contact-container">
							<?php while( have_rows('contact_column') ): the_row(); 
								// vars
								$column_title = get_sub_field('column_title');
								$column_description = get_sub_field('column_description');
								if ( get_sub_field('use_button') == 'yes' ) {
									$button = 'yes';
								}
								$button_text = get_sub_field('button_text');
								$button_link = get_sub_field('button_link');
								?>
								<div class="support-column">
								<?php if( $column_title ): ?>
									<h5><?php echo $column_title; ?></h5>
								<?php endif; ?>
								<?php if( $column_description ): ?>
								    <?php echo $column_description; ?>
								<?php endif; ?>
								<?php if( $button ): ?>
									<?php if( $button_link ): ?>
									<a href="<?php echo $button_link; ?>" class="btn large"><?php echo $button_text; ?></a>
									<?php endif; ?>
								<?php endif; ?>
								</div>
							<?php endwhile; ?>
						</div>
					<?php endif; ?>
					</div>
				</section>

			<?php endwhile; else : endif; ?>

		</div>

<?php get_footer(); ?>