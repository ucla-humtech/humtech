<?php
/*
 Template Name: People Listing
*/
?>
<?php get_header(); ?>
			<header id="main-content" class="main">
				<div class="content">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
					<?php // Select what people category to show
					$people_category = get_field('people_category');
					if( $people_category ) {
						$people_cat = $people_category->slug;
					}
					// Set varaibles to decide behavior of page
					if ( get_field('link_to_pages') == 'yes' ) {
						$person_link = 'yes';
					}
					$people_details = get_field('people_details');
					if( in_array('position', $people_details) ) { 
						$position = 'yes';
					} 
					if( in_array('interest', $people_details) ) {
						$interest = 'yes';
					} 
					if( in_array('email', $people_details) ) {
						$email = 'yes';
					}
					if( in_array('phone', $people_details) ) {
						$phone = 'yes';
					} 
					?>
				</div>
				<?php if ( get_field('display_filters') == 'show' ) { ?>
				<?php if ( has_nav_menu('faculty-filter') ) { ?> 
				<div class="filter">
				<?php // To make another filter, duplicate the div below ?>
					<div class="options button-group" data-filter-group="field">
					<?php if(get_field('filter_label')) { ?>
						<h3><?php the_field('filter_label'); ?></h3>
					<?php } ?>
						<button data-filter="" data-text="All" data-description="" class="option all is-checked">All</button>
						<?php wp_nav_menu(array(
							'container' => false,
							'menu' => __( 'People Filter', 'bonestheme' ),
							'menu_class' => 'faculty-filter',
							'theme_location' => 'faculty-filter',
							'before' => '',
							'after' => '',
							'depth' => 1,
							'items_wrap' => '%3$s',
							'walker' => new Filter_Walker
						)); ?>
					</div>
				</div>
				<?php wp_nav_menu( array(
					'menu' => 'People Filter',
					'menu_class' => 'faculty-filter',
					'items_wrap' => '<form class="dropdown-filter"><select><option value="*">View All</option>%3$s</select></form>',
					'walker' => new Dropdown_Walker()
				)); ?>
				<h2 class="filter-title">All</h2>
				<p class="filter-description">&nbsp;</p>
				<?php } 
			} ?>
			</header>
			<div class="content">
				<div class="filter-list">
					<ul class="<?php echo $people_cat ?>">
					<?php $core_loop = new WP_Query( array( 'people_cat' => $people_cat, 'post_type' => 'people', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC')); ?>
					<?php while ( $core_loop->have_posts() ) : $core_loop->the_post(); ?>
						<li class="filter-item<?php $areas = get_field('areas_of_focus'); if( $areas ): foreach( $areas as $area ): ?> <?php echo $area->slug; ?><?php endforeach; endif;?><?php if ( $person_link == 'yes' ) { ?> hover<?php } ?>">
							<?php if ( $person_link == 'yes' ) { ?>
							<a href="<?php the_permalink() ?>">
							<?php } ?>
								<?php // if there is a photo, use it
								if(get_field('photo')) {
									$image = get_field('photo');
									if( !empty($image) ): 
										// vars
										$url = $image['url'];
										$title = $image['title'];
										// thumbnail
										$size = 'people-large';
										$thumb = $image['sizes'][ $size ];
										$width = $image['sizes'][ $size . '-width' ];
										$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php // otherwise use a silhouette
								} else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php } ?>
								<?php // If this pages uses email and links to pages
								if ( $person_link == 'yes' && $email == 'yes' ) { ?>
								</a>
								<?php } ?>
								<dl>
									<dt class="name">
										<?php // If this pages uses email and links to pages
										if ( $person_link == 'yes' && $email == 'yes' ) { ?>
										<a href="<?php the_permalink() ?>">
										<?php } ?><?php the_title(); ?><?php if ( $person_link == 'yes' && $email == 'yes' ) { ?>
										</a>
										<?php } ?>
									</dt>
									<?php 
									if ( $position == 'yes' ) {
									if(get_field('position_title')) { ?>
									<dd class="position"><?php the_field('position_title'); ?></dd>
									<?php }
									}
									if ( $email == 'yes' ) { 
									if(get_field('email_address')) {
										$person_email = antispambot(get_field('email_address')); ?>
									<dd class="email">
										<a href="mailto:<?php echo $person_email; ?>"><?php echo $person_email; ?></a>
									</dd>
									<?php }
									}
									if ( $phone == 'yes' ) { 
									if(get_field('phone_number')) { ?>
									<dd class="phone"><?php the_field('phone_number'); ?></dd>
									<?php } 
									}
									if ( $interest == 'yes' ) {
									if(get_field('short_summary')) { ?>
									<dd class="interest"><?php the_field('short_summary'); ?></dd>
									<?php }
									} ?>
								</dl>
							<?php if ( $person_link == 'yes' ) { ?>
							</a>
							<?php } ?>
						</li>
					<?php endwhile; ?>					
					</ul>					
				</div>
			</div>
<?php get_footer(); ?>