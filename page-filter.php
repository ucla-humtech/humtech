<?php
/*
 Template Name: Filter Page
*/
?>
<?php get_header(); ?>

			<div class="content">
				<div class="col" id="main-content" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1><?php the_title(); ?></h1>
						<section>
							<?php the_content(); ?>
						</section>
						<section>
							<?php if( have_rows('filter_options') ): ?>
							<div class="filter">
								<div class="options button-group" data-filter-group="field">
									<button data-filter="" data-text="All" data-description="" class="option all is-checked">All</button>
								<?php while( have_rows('filter_options') ): the_row(); 
									$option_title = get_sub_field('option_title');
								?>
								<?php if( $option_title ): ?>
									<button data-filter=".<?php echo $option_title; ?>" data-text="<?php echo $option_title; ?>" class="option <?php echo $option_title; ?>"><?php echo $option_title; ?></button>
								<?php endif;  endwhile; ?>
						
								</div>
							</div>
							<form class="dropdown-filter">
								<select>
									<option value="*">View All</option>
									<?php while( have_rows('filter_options') ): the_row(); 
										$option_title = get_sub_field('option_title');
									?>
									<?php if( $option_title ): ?>
									<option value=".<?php echo $option_title; ?>"><?php echo $option_title; ?></option>
									<?php endif;  endwhile; ?>
								</select>
							</form>
							<?php endif; ?>
							<?php if( have_rows('items') ): ?>
							<ul class="filter-list">
								<?php while( have_rows('items') ): the_row(); 
									$item_title = get_sub_field('item_title');
									$item_description = get_sub_field('item_description');
									$item_category = get_sub_field('item_category');
									$item_link = get_sub_field('item_link');
									$item_image = get_sub_field('item_image');
									$item_image_size = 'speaker-photo';
								?>
								<li class="filter-item <?php echo $item_category; ?>">
									<?php if( $item_link ): ?><a href="<?php echo $item_link; ?>"><?php endif; ?>
										<?php echo wp_get_attachment_image( $item_image, $item_image_size ); ?>
										<dl>
											<?php if( $item_title ): ?>
											<dt><?php echo $item_title; ?></dt>
											<?php endif; ?>
											<?php if( $item_description ): ?>
											<dd><?php echo $item_description; ?></dd>
											<?php endif; ?>
										</dl>
									<?php if( $item_link ): ?></a><?php endif; ?>
								</li>
								<?php endwhile; ?>
							</ul>
							<?php endif; ?>
						</section>
					</article>

				<?php endwhile; else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

				<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>